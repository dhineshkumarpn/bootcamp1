﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetector : MonoBehaviour
{

    float speed;
    bool isSleep;
    void LateUpdate()
    {
        speed = transform.GetComponent<Rigidbody>().velocity.magnitude;
        if (speed < 0.5f)
        {
            transform.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        }
        isSleep= transform.GetComponent<Rigidbody>().IsSleeping();
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (transform.gameObject.tag == collision.gameObject.tag)
        {
            Debug.Log(transform.gameObject.tag +" tags "+collision.gameObject.tag);
            if (isSleep)
            {
                Destroy(transform.gameObject);
            }
        }
    }
}
