﻿using Doozy.Engine.Events;
using Doozy.Engine.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MenuBuilder : MonoBehaviour
{
    public GameObject menuItemPrefab;
    public GameObject ParentPanel;
    public string[] shapes;
    public GameObject TopPanel;

    public GameObject spwn;

    int spawncount = 0;
    void Start()
    {
        InstantiateButtons();

    }

    public void InstantiateButtons()
    {
        for (int i = 0; i < shapes.Length; i++)
        {
            GameObject button = Instantiate(menuItemPrefab, ParentPanel.transform);
            button.tag = shapes[i];
            button.GetComponentInChildren<TextMeshProUGUI>().text = shapes[i];
            button.name = shapes[i];
            Button tempbutton = button.GetComponent<Button>();
            tempbutton.onClick.AddListener(() => ButtonClicked(button.name));
        }
    }

    public void ButtonClicked(string buttonName)
    {
        int spawnnum = 0;
       

        if (buttonName.CompareTo("Cube") == 0)
        {
            spawnnum = 1;
        }
        else
        {
            spawnnum = 2;
        }
        spwn.GetComponent<SpawnManager>().SetSpawn(buttonName);
        spwn.GetComponent<SpawnManager>().SpawnInside(spawnnum);
        bool change = CheckSpawned(buttonName);
        if (change)
        {
            UnhighlightObjects();
            HighlightObjects(buttonName);
        }
    }

    public bool CheckSpawned(string buttonname)
    {
        bool result=false;

        GameObject[] Go = GameObject.FindGameObjectsWithTag(buttonname);
        if (Go.Length > 0)
        {
            //result = true;
        }
        return result;
    }
    public void HighlightObjects(string name)
    {
        GameObject[] go = GameObject.FindGameObjectsWithTag(name);
        foreach (GameObject g in go)
        {
            if (g.GetComponent<Renderer>() != null)
            {
                g.GetComponent<Renderer>().material.color = Color.yellow;
            }
            
        }
    }

    public void UnhighlightObjects()
    { 
        GameObject[] cubes = GameObject.FindGameObjectsWithTag("Cube");
        GameObject[] spheres = GameObject.FindGameObjectsWithTag("Sphere");

        foreach (GameObject g in cubes)
        {
            if (g.GetComponent<Renderer>() != null)
            {
                g.GetComponent<Renderer>().material.color = new Color(209, 44, 44);
            }
                
        }

        foreach (GameObject g in spheres)
        {
            if (g.GetComponent<Renderer>() != null)
            {
                g.GetComponent<Renderer>().material.color = new Color(99, 41, 204);
            }
               
        }
    }

}
