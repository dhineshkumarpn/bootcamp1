﻿using Doozy.Engine.UI;
using Microsoft.Unity.VisualStudio.Editor;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TopPanel : MonoBehaviour
{
    public Sprite[] Sprites;

    public GameObject Counter;
    public GameObject AddButton;

    public GameObject SpawnManager;
    SpawnManager spawnMgr;


    private void Start()
    {
        spawnMgr = SpawnManager.GetComponent<SpawnManager>();
    }
    public void SetSprite(int selection)
    {

    }
    public void SetCounter(int count)
    {
        Counter.GetComponent<TextMeshProUGUI>().text = count.ToString();
    }

    public void AddObjects(string objectname)
    {
        int spawnNum = 0;
        if (objectname == "Cube")
        {
            spawnNum = 1;
        }
        else
        {
            spawnNum = 2;
        }

        //AddButton.GetComponent<UIButton>().OnClick.OnTrigger.Event = spawnMgr.SpawnInside(spawnNum);
    }

    //private void OnEnable()
    //{
    //    Message.AddListener<GameEventMessage>(OnMessage);

    //}

    //private void OnDisable()
    //{
    //    Message.RemoveListener<GameEventMessage>(OnMessage);

    //}

    //private void OnMessage(GameEventMessage message)
    //{

    //}
}
