﻿using Doozy.Engine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject CubePrefab;
    public GameObject SpherePrefab;
    public GameObject SpawnArea;

    public float dimX;
    public float dimY;

    int SpawnCount = 0;
    bool Spawnflag = false;
    List<Vector3> PrevSpawn = new List<Vector3>();

    int SpawnGO;
    void Start()
    {
        RectTransform rect = SpawnArea.transform.GetComponent<RectTransform>();
        dimX = rect.sizeDelta.x;
        dimY = rect.sizeDelta.y;

    }
    public void SpawnInside(int spawnnum)
    {
        GameObject spawnObject;

        if (spawnnum == 1)
        {
            spawnObject = CubePrefab;
        }
        else
        {
            spawnObject = SpherePrefab;
        }
        
        Vector3 randpos = Vector3.zero;
        randpos.x = Random.Range(-dimX , dimX);
        randpos.y = Random.Range(-dimY , dimY);
        randpos.z = 0f;
        if (SpawnCount == 0)
        {
            Spawnflag = true;
        }
        else if (PrevSpawn.Contains(randpos))
        {
            Spawnflag = false;
            SpawnInside(spawnnum);
        }
        if (Spawnflag & SpawnCount<10)
        {
            
            Transform instance = Instantiate(spawnObject, SpawnArea.transform).transform;
            SpawnCount++;
            instance.localPosition = randpos;
            PrevSpawn.Add(randpos);
        }
        
    }
    public void SetSpawn(string name)
    {
        if (name == "Cube")
        {
            SpawnGO = 1;
        }
        else
        {
            SpawnGO = 2;
        }
    }

    public int GetspawnCount()
    {
        return SpawnCount;
    }

}
